package org.filho.modernart;

import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.Toast;

import org.filho.modernart.util.Colors;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private List<ImageView> rectangles = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        rectangles.add((ImageView) findViewById(R.id.rectangle1));
        rectangles.add((ImageView) findViewById(R.id.rectangle2));
        rectangles.add((ImageView) findViewById(R.id.rectangle3));
        rectangles.add((ImageView) findViewById(R.id.rectangle4));
        rectangles.add((ImageView) findViewById(R.id.rectangle5));

        SeekBar seekBar = (SeekBar) findViewById(R.id.seekBar);

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if(fromUser)
                    changeColors(progress, rectangles);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) { }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) { }
        });
    }

    private void changeColors(int progress, List<ImageView> rectangles) {
        for (ImageView view : rectangles) {
            int currentColor = ColorDrawable.class.cast(view.getDrawable()).getColor();

            int colorMultiplier = getColorMultiplier(view.getId());

            // Filters the color
            int transformed = currentColor + (progress * colorMultiplier);

            view.setColorFilter(transformed, PorterDuff.Mode.LIGHTEN);
//            view.getDrawable().mutate().setColorFilter(currentColor, PorterDuff.Mode.MULTIPLY);
//            view.setImageDrawable(new ColorDrawable(currentColor+progress*5));

            if(view.getId() == R.id.rectangle4) {
                Log.i("Modern", String.format("Current color [%s], transformed [%s]", Colors.toHex(currentColor), Colors.toHex(transformed)));
                Log.i("Modern", String.format("Current color [%s], progress [%s], multiplier [%s]",
                        Colors.toHex(currentColor),
                        progress,
                        colorMultiplier));
            }
        }
    }

    private int getColorMultiplier(int viewId) {
        switch (viewId) {
            case R.id.rectangle1: return 1;
            case R.id.rectangle2: return 2;
            case R.id.rectangle4: return 3;

            default: return 4;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.top_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.menu_information:
                AlertDialogFragment.newInstance().show(getFragmentManager(), "MoreInfo");
        }

        return super.onOptionsItemSelected(item);
    }
}
